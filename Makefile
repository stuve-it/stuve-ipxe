
.PHONY: clean mrproper all imgs certs install_kernel install_server install_admin test
noop=
space= $(noop) $(noop)
comma= ,

ifndef PASS
$(error "set PASS in environment")
endif
ALTMBR ?= $(shell locate --existing --basename altmbr.bin)

STUVE_HOSTS = alfa bravo charlie delta echo foxtrot golf hotel india juliett kilo lima mike november
IMGS = $(addprefix out/,$(addsuffix .stuve.lmu.de.img,$(STUVE_HOSTS)))
GAF_HOSTS = left right up down
IMGS += $(addprefix out/,$(addsuffix .gaf.fs.lmu.de.img,$(GAF_HOSTS)))

all:
	@echo "targets are imgs or certs"

imgs: $(IMGS)

certs:	cert/pxe.fs.lmu.de.crt cert/admin.pfx cert/codesigning.crt

upload_cert: cert/pxe.fs.lmu.de.crt cert/ca.key out/admin.ipxe.sig
	cat cert/pxe.fs.lmu.de.crt cert/ca.crt \
	| ssh root@gabor "cat > /etc/ssl/de.lmu.fs.pxe.crt.combined"
	openssl rsa -in cert/pxe.fs.lmu.de.key -passin pass:$(PASS) \
	| ssh root@gabor "cat > /etc/ssl/private/de.lmu.fs.pxe.key"
	scp cert/ca.crt root@gabor:/etc/ssl/de.lmu.fs.pxe-ca.crt
	ssh root@gabor /etc/init.d/nginx restart
	scp out/admin.ipxe out/admin.ipxe.sig root@static:/srv/http/de.lmu.fs.pxe/script/
	curl --cacert cert/ca.crt --cert cert/admin.crt --key cert/admin.key --pass $(PASS) \
		https://pxe.fs.lmu.de/script/ > /dev/null

out/part%.mbr:
	echo -ne "\x$(subst .mbr,,$(subst out/part,,$@))" | cat $(ALTMBR) - > $@

logo/%.png:
	if [ -n "$(findstring stuve.lmu.de,$@)" ] ; then \
		if [ -n "$(findstring november.stuve.lmu.de,$@)" ] ; then \
			cp logo/stuve_800_600.png $@ ; \
		else \
			cp logo/stuve_1280_720.png $@ ; \
		fi ; \
	elif [ -n "$(findstring gaf.fs.lmu.de,$@)" ] ; then \
		cp logo/gaf_1280_720.png $@ ; \
	elif [ -n "$(findstring l.xmw.de,$@)" ] ; then \
		if [ -n "$(findstring mojito.l.xmw.de,$@)" ] ; then \
			cp logo/xmw_1366_768.png $@ ; \
		else \
			echo $@ not implemented ; \
			exit -1 ; \
		fi ; \
	else \
		echo $@ not implemented ; \
		exit -1 ; \
	fi

out/%-embedded.ipxe: Makefile
	echo "#!ipxe"							 > $@
	echo "imgtrust --permanent || goto error"			>> $@
	echo "imgverify --signer codesigning grub.exe grub.exe.sig || goto error"	>> $@
	
	echo "console --picture $(subst out/,,$(subst -embedded.ipxe,.png,$@)) || goto error" >> $@ ; \
	if [ -n "$(findstring stuve.lmu.de,$@)" ] ; then \
		echo "cpair --foreground 7 --background 4 4"		>> $@ ; fi
	if [ -n "$(findstring gaf.fs.lmu.de,$@)" ] ; then \
		echo "colour --rgb 0x14c714 1"				>> $@ ; \
		echo "cpair --foreground 0 --background 1 2"		>> $@ ; \
		echo "cpair --foreground 0 --background 4 4"		>> $@ ; fi
	if [ -n "$(findstring xmw.de,$@)" ] ; then \
		echo "cpair --foreground 7 --background 4 4"		>> $@ ; fi
	
	echo ":loop"							>> $@
	echo "ifconf --configurator ipv6 net0 || goto nonet"		>> $@
	echo "set dns6 2001:4ca0::53:1 || goto nonet"			>> $@
	echo "set target host"						>> $@
	echo "goto menu"						>> $@
	echo ":nonet"							>> $@
	echo "set target fallback"					>> $@
	echo ":menuwait"						>> $@
	echo "prompt --timeout 2000 wait for 2s."			>> $@
	echo ":menu"							>> $@
	echo "menu Welcome to $(subst out/,,$(subst -embedded.ipxe,,$@)) v0.2"	>> $@
	echo "item host Regular boot"					>> $@
	echo "item admin Maintainance mode"				>> $@
	echo "item fallback Fallback mode"				>> $@
	echo "item reboot Reboot"					>> $@
	echo "item poweroff Power off"					>> $@
	if [ -n "${DEBUG_SHELL}" ] ; then \
		echo "item shell Shell"					>> $@ ; \
	fi
	echo "choose --timeout 10000 --default \$${target} target || goto menu"	>> $@
	echo "goto \$${target} || goto error"				>> $@
	
	echo ":host"							>> $@
	echo "imgfetch --name host.ipxe https://pxe.fs.lmu.de/script/$(subst out/,,$(subst -embedded,,$@)) || goto fallback"	>> $@
	echo "imgverify --signer codesigning host.ipxe https://pxe.fs.lmu.de/script/$(subst out/,,$(subst -embedded,,$@)).sig || goto error"	>> $@
	echo "imgexec host.ipxe || goto error"				>> $@
	
	echo ":admin"							>> $@
	echo "login || goto loop"					>> $@
	echo "imgfetch https://\$${username:uristring}:\$${password:uristring}@pxe.fs.lmu.de/script/admin.ipxe || goto error"	>> $@
	echo "imgverify --signer codesigning admin.ipxe https://\$${username:uristring}:\$${password:uristring}@pxe.fs.lmu.de/script/admin.ipxe.sig || goto error"	>> $@
	echo "imgexec admin.ipxe || goto error"				>> $@
	
	echo ":fallback"						>> $@
	echo "echo Fallback enviroment"					>> $@
	if [ -n "$(findstring stuve.lmu.de,$@)" ] ; then \
		echo "imgargs grub.exe --config-file=root (hd0,1) \|\| reboot \; chainloader /bootmgr \|\| reboot || goto error"	>> $@ ; \
		echo "imgexec grub.exe || goto error"				>> $@ ; \
	elif [ -n "$(findstring gaf.fs.lmu.de,$@)" ] ; then \
		echo "imgfree || goto error"				>> $@ ; \
		echo "imgfetch https://pxe.fs.lmu.de/kernel/vmlinuz hostname=$(subst out/,,$(subst .gaf.fs.lmu.de-embedded.ipxe,,$@)) || goto error"	>> $@ ; \
		echo "imgverify --signer codesigning vmlinuz https://pxe.fs.lmu.de/kernel/vmlinuz.sig || goto error"	>> $@ ; \
		echo "imgfetch https://pxe.fs.lmu.de/kernel/initrd || goto error"		>> $@ ; \
		echo "imgverify --signer codesigning initrd https://pxe.fs.lmu.de/kernel/initrd.sig || goto error"	>> $@ ; \
		echo "imgexec vmlinuz || goto error"			>> $@ ; \
	fi
	
	if [ -n "${DEBUG_SHELL}" ] ; then \
		echo ":shell"						>> $@ ; \
		echo "shell"						>> $@ ; \
	fi
	echo ":error"							>> $@
	echo "echo An error occurred"					>> $@
	echo "imgstat"							>> $@
	echo ":reboot"							>> $@
	echo "prompt --timeout 5000 reboot in 5s."				>> $@
	echo "reboot || goto menuwait"					>> $@
	echo ":poweroff"						>> $@
	echo "prompt --timeout 5000 Power off in 5s."			>> $@
	echo "poweroff || goto menuwait"				>> $@

out/admin.ipxe: Makefile
	echo "#!ipxe"							 > $@
	echo "colour --rgb 0x14c714 1"					>> $@
	echo "cpair --foreground 1 0"					>> $@
	echo "echo Admin enviroment"					>> $@
	echo ":menu"							>> $@
	echo "menu Welcome to the dark side"				>> $@
	echo "item winpe10 WinPE 10"					>> $@
	echo "item winpe7 WinPE 7"					>> $@
	echo "item second boot from (hd1) / usb stick"			>> $@
	echo "item shell shell"						>> $@
	echo "choose target || goto menu"				>> $@
	echo "goto \$${target}"						>> $@
	
	echo ":winpe10"							>> $@
	echo "imgfree"							>> $@
	echo "imgfetch /wimboot.x86_64.unsigned || goto error"	>> $@
	echo "imgverify --signer codesigning wimboot.x86_64.unsigned /wimboot.x86_64.unsigned.sig || goto error"	>> $@
	echo "imgfetch /winpe10/bcd BCD || goto error"		>> $@
	echo "imgverify --signer codesigning bcd /winpe10/bcd.sig || goto error"	>> $@
	echo "imgfetch /winpe10/boot.sdi boot.sdi || goto error"	>> $@
	echo "imgverify --signer codesigning boot.sdi /winpe10/boot.sdi.sig || goto error"	>> $@
	echo "imgfetch /winpe10/boot.wim boot.wim || goto error"	>> $@
	echo "imgverify --signer codesigning boot.wim /winpe10/boot.wim.sig || goto error"	>> $@
	echo "imgexec wimboot.x86_64.unsigned || goto error"		>> $@
	
	echo ":winpe7"							>> $@
	echo "imgfree"							>> $@
	echo "imgfetch /wimboot.x86_64.unsigned || goto error"		>> $@
	echo "imgverify --signer codesigning wimboot.x86_64.unsigned /wimboot.x86_64.unsigned.sig || goto error"	>> $@
	echo "imgfetch /winpe7/bcd BCD || goto error"			>> $@
	echo "imgverify --signer codesigning bcd /winpe7/bcd.sig || goto error"	>> $@
	echo "imgfetch /winpe7/boot.sdi boot.sdi || goto error"		>> $@
	echo "imgverify --signer codesigning boot.sdi /winpe7/boot.sdi.sig || goto error"	>> $@
	echo "imgfetch /winpe7/boot.wim boot.wim || goto error"		>> $@
	echo "imgverify --signer codesigning boot.wim /winpe7/boot.wim.sig || goto error"	>> $@
	echo "imgexec wimboot.x86_64.unsigned || goto error"		>> $@
	
	echo ":second"							>> $@
	echo "imgexec grub.exe --config-file=\"root (hd1);chainloader +1\" || goto error"	>> $@
	echo ":error"							>> $@
	echo "echo An error occured."					>> $@
	echo "prompt --timeout 10 Press any key for shell"		>> $@
	echo ":shell"							>> $@
	echo "shell"							>> $@
	echo "goto menu"						>> $@

upload_admin: out/admin.ipxe.sig
	scp out/admin.ipxe out/admin.ipxe.sig root@static:/srv/http/de.lmu.fs.pxe/script/

check_admin: out/admin.ipxe cert/admin.crt cert/admin.key
	curl --cacert cert/ca.crt --cert cert/admin.crt --key cert/admin.key --pass $(PASS) \
		--user webermi https://pxe.fs.lmu.de/script/admin.ipxe > out/admin.ipxe.server
	diff out/admin.ipxe.server out/admin.ipxe
	rm out/admin.ipxe.server

out/%.sig: out/% cert/codesigning.crt
	openssl cms \
		-passin pass:$(PASS) \
		-sign \
		-binary \
		-noattr \
		-in $< \
		-signer cert/codesigning.crt \
		-inkey cert/codesigning.key \
		-certfile cert/ca.crt \
		-outform DER \
		-out $@

out/%.img: cert/%.crt out/%-embedded.ipxe ipxe/src/config/local/general.h ipxe/src/config/local/console.h logo/%.png cert/ca.crt out/grub.exe.sig
	mkdir -p ipxe/src/bin
	# avoid prompt for passphrease during make
	echo ../../$(subst .crt,.key,$<) > ipxe/src/bin/.private_key.list
	openssl rsa \
		-passin pass:$(PASS) \
		-in $(subst .crt,.key,$<) \
		-outform DER \
		-out ipxe/src/bin/.private_key.der
	$(MAKE) $(MAKEFLAGS) \
		-C ipxe/src \
		bin/ipxe.hd \
		EMBED=$(subst $(space),$(comma),$(addprefix ../../,$(subst .img,-embedded.ipxe,$@) $(subst out/,logo/,$(subst .img,.png,$@)) out/grub.exe out/grub.exe.sig)) \
		TRUST=../../cert/ca.crt \
		CERT=../../$(subst out/,cert/,$(subst .img,.crt,$@)) \
		PRIVKEY=../../$(subst out/,cert/,$(subst .img,.key,$@))
	#	ARCH=i386 \
	#	PLATFORM=pcbios
	mkdir -p out
	mv ipxe/src/bin/ipxe.hd $@

ipxe/src/config/local/general.h: ipxe/src/config/general.h Makefile
	echo ""					 > $@
	echo "#define NET_PROTO_IPV6"		>> $@
	echo "#define NET_PROTO_IPV4"		>> $@
	echo "#undef NET_PROTO_STP"		>> $@
	echo ""					>> $@
	echo "#define DOWNLOAD_PROTO_HTTPS"	>> $@
	echo "#undef DOWNLOAD_PROTO_TFTP"	>> $@
	echo "#define IMAGE_PXE"		>> $@
	echo "#undef SANBOOT_PROTO_AOE"		>> $@
	echo "#undef SANBOOT_PROTO_ISCSI"	>> $@
	echo "#undef SANBOOT_PROTO_IB_SRP"	>> $@
	echo "#define IMAGE_TRUST_CMD"		>> $@
	echo "#define REBOOT_CMD"		>> $@
	echo "#define POWEROFF_CMD"		>> $@
	echo "#define CONSOLE_CMD"		>> $@
	echo "#define IMAGE_PNG"		>> $@

ipxe/src/config/local/console.h: ipxe/src/config/console.h Makefile
	echo "#define CONSOLE_FRAMEBUFFER"	>> $@
	echo "#define CONSOLE_INT13"		>> $@

ipxe/src/config/general.h:
	git clone git://git.ipxe.org/ipxe.git ipxe

cert/ca.crt:
	echo -e "DE\nBayern\nMuenchen\nStuVe LMU\nReferat IT\n\nroot@fs.lmu.de" \
		| openssl req -x509 \
		-passout pass:$(PASS) \
		-newkey rsa:2048 \
		-out cert/ca.crt \
		-keyout cert/ca.key \
		-days 3650
	echo 01 > cert/ca.srl
	touch cert/ca.idx

.SUFFIXES: .crt .key

cert/%.req:
	echo -e "DE\nBayern\nMuenchen\nStuVe LMU\nReferat IT\n$(subst .req,,$(subst cert/,,$@))\nroot@fs.lmu.de\nStuVe LMU\n" \
	| openssl req \
		-passin pass:$(PASS) \
		-passout pass:$(PASS) \
		-newkey rsa \
		-out $@ \
		-keyout $(subst req,key,$@)

cert/%.crt: cert/%.req cert/ca.key
	echo -e "y\ny" \
	| openssl ca \
		-passin pass:$(PASS) \
		-config cert/ca.cnf \
		-in $(subst crt,req,$@) \
		-out $@ \
		-days 3650

cert/codesigning.crt: cert/codesigning.req cert/ca.key
	echo -e "y\ny" \
	| openssl ca \
		-passin pass:$(PASS) \
		-config cert/ca.cnf \
		-extensions codesigning \
		-in $(subst crt,req,$@) \
		-out $@ \
		-days 3650

cert/%.pfx: cert/%.key cert/%.crt
	openssl pkcs12 \
		-passin pass:$(PASS) \
		-passout pass:$(PASS) \
		-inkey $(subst .pfx,.key,$@) \
		-in $(subst .pfx,.crt,$@) \
		-export \
		-out $@

out/grub.exe:
	wget "http://downloads.sourceforge.net/project/grub4dos/GRUB4DOS/grub4dos%200.4.4/grub4dos-0.4.4.zip" -P out
	unzip out/grub4dos-0.4.4.zip -d out
	cp out/grub4dos-0.4.4/grub.exe $@
	rm -r out/grub4dos-0.4.4.zip out/grub4dos-0.4.4

LOOP := $(shell losetup --find)
TMP := $(shell mktemp /tmp/XXXXX.disk)

out/%.cfg: Makefile
	echo "timeout 0"			 > $@
	echo "default $(subst out/,,$(subst .cfg,.img,$@))"	>> $@
	echo "label $(subst out/,,$(subst .cfg,.img,$@))"		>> $@
	echo "  kernel /$(subst out/,,$(subst .cfg,.img,$@))"	>> $@

out/%.disk: out/%.img out/%.cfg out/part4.mbr out/%.cfg
	yes | dd iflag=fullblock bs=1M count=20 of=$(TMP)
	echo -ne "o\nn\np\n1\n\n+10M\nn\np\n4\n\n\nt\n1\nc\np\nw\ny\n" | fdisk $(TMP) || true
	kpartx -av $(TMP)
	sleep 2
	mkfs.vfat $(subst /dev/,/dev/mapper/,$(LOOP))p4
	mkdir $(TMP).mount
	mount $(subst /dev/,/dev/mapper/,$(LOOP))p4 $(TMP).mount
	cp $(subst .disk,.img,$@) $(TMP).mount/
	cp $(subst .disk,.cfg,$@) $(TMP).mount/syslinux.cfg
	df -h $(TMP).mount
	syslinux --install $(subst /dev/,/dev/mapper/,$(LOOP))p4
	umount $(TMP).mount
	pv out/part4.mbr > $(LOOP)
	kpartx -dv $(LOOP)
	losetup -d $(LOOP)
	mv $(TMP) $@
	qemu-system-x86_64 -drive file=$@,format=raw

winmbr_%.stuve.lmu.de: out/win.mbr
	dd if=$< bs=440 count=1 | ssh $(subst .stuve.lmu.de,,$(subst ipxembr_,,$@)) "dd of=/dev/sda bs=440"

ipxembr_%.stuve.lmu.de: out/%.stuve.lmu.de.img
	dd if=$< bs=440 count=1 | ssh $(subst .stuve.lmu.de,,$(subst ipxembr_,,$@)) "dd of=/dev/sda bs=440"

ipxembr_%.gaf.fs.lmu.de: out/%.gaf.fs.lmu.de.img
	dd if=$< bs=440 count=1 | ssh $(subst .gaf.fs.lmu.de,,$(subst ipxembr_,,$@)) "dd of=/dev/sda bs=440"

deploy_%: out/%.img
	dd if=$< bs=512 | ssh $(subst .stuve.lmu.de,,$(subst .gaf.fs.lmu.de,,$(subst .l.xmw.de,,$(subst deploy_,,$@)))) "dd of=/dev/sda1 bs=512"

activate_%:
	ssh $(subst .stuve.lmu.de,,$(subst .gaf.fs.lmu.de,,$(subst .l.xmw.de,,$(subst activate_,,$@)))) "sfdisk --activate /dev/sda 1"

out/wimboot:
	rsync -avP static:/srv/http/de.lmu.fs.pxe/winpe/wimboot $@
out/bcd:
	rsync -avP static:/srv/http/de.lmu.fs.pxe/winpe/boot/bcd $@
out/boot.sdi:
	rsync -avP static:/srv/http/de.lmu.fs.pxe/winpe/boot/boot.sdi $@
out/winpe64.wim:
	rsync -avP static:/srv/http/de.lmu.fs.pxe/winpe/wim/winpe64.wim $@

install_pesig: out/wimboot.sig out/bcd.sig out/boot.sdi.sig out/winpe64.wim.sig
	rsync -avP out/wimboot.sig	static:/srv/http/de.lmu.fs.pxe/winpe/
	rsync -avP out/bcd.sig		static:/srv/http/de.lmu.fs.pxe/winpe/boot/
	rsync -avP out/boot.sdi.sig	static:/srv/http/de.lmu.fs.pxe/winpe/boot/
	rsync -avP out/winpe64.wim.sig	static:/srv/http/de.lmu.fs.pxe/winpe/wim/

clean:
	make -C ipxe/src clean
	rm -f ipxe/src/config/local/general.h ipxe/src/config/local/console.h
	rm -f out/*.de.img

mrproper: clean
	rm -rf ipxe
	rm -f cert/*.key cert/*.req cert/*.crt cert/ca.idx* cert/ca.srl*
